export const constantEval = ({ types: t }) => ({
  visitor: {
    MemberExpression(path) {
      const { node } = path
      if (!t.isIdentifier(node.object) || !t.isIdentifier(node.property)) return
      const {
        object: { name: object },
        property: { name: property },
      } = node
      if (object === "Node") {
        if (property === "ELEMENT_NODE") {
          path.replaceWith(t.numericLiteral(1))
        } else if (property === "TEXT_NODE") {
          path.replaceWith(t.numericLiteral(3))
        }
      } else if (object === "NodeFilter") {
        if (property === "FILTER_REJECT") {
          path.replaceWith(t.numericLiteral(2))
        } else if (property === "FILTER_ACCEPT") {
          path.replaceWith(t.numericLiteral(1))
        } else if (property === "SHOW_TEXT") {
          path.replaceWith(t.numericLiteral(4))
        }
      }
    },
  },
})
