/* eslint-disable */
/**
 * MIT License
 * Copyright (c) 2014-present Sebastian McKenzie and other contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
"use strict"

function _interopDefault(ex) {
  return ex && typeof ex === "object" && "default" in ex ? ex["default"] : ex
}

var template = _interopDefault(require("@babel/template"))

var buildForOfArray = template(`
  for (var KEY = 0; KEY < ARR.length; KEY++) BODY;
`)
function ForOfStatementArray(path, t) {
  var { node: node, scope: scope } = path
  var nodes = []
  var right = node.right
  if (!(t.isIdentifier(right) && scope.hasBinding(right.name))) {
    var uid = scope.generateUidIdentifier("arr")
    var decl = t.variableDeclarator(uid, right)
    nodes.push(t.variableDeclaration("var", [decl]))
    right = uid
  }
  var iterationKey = scope.generateUidIdentifier("i")
  var iter = buildForOfArray({ BODY: node.body, KEY: iterationKey, ARR: right })
  t.inherits(iter, node)
  t.ensureBlock(iter)
  var iterationValue = t.memberExpression(right, iterationKey, true)
  var left = node.left
  if (t.isVariableDeclaration(left)) {
    left.declarations[0].init = iterationValue
    iter.body.body.unshift(left)
  } else {
    var assignment = t.assignmentExpression("=", left, iterationValue)
    iter.body.body.unshift(t.expressionStatement(assignment))
  }
  if (path.parentPath.isLabeledStatement()) {
    nodes.push(t.labeledStatement(path.parentPath.node.label, iter))
  } else {
    nodes.push(iter)
  }
  return nodes
}
var forOfLoose = function () {
  return function ({ types: t }) {
    return {
      visitor: {
        ForOfStatement: function (path) {
          if (path.parentPath.isLabeledStatement()) {
            path.parentPath.replaceWithMultiple(ForOfStatementArray(path, t))
          } else {
            path.replaceWithMultiple(ForOfStatementArray(path, t))
          }
        },
      },
    }
  }
}

module.exports = forOfLoose
