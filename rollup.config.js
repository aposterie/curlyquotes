import babel from "@rollup/plugin-babel"
import prettier from "rollup-plugin-prettier"
import node from "@rollup/plugin-node-resolve"
import typescript from "rollup-plugin-typescript2"
import { constantEval } from "./scripts/constants"

const extensions = [".js", ".ts"]

export default {
  input: "./src/index.ts",
  output: [{ file: "dist/curlyquotes.js", format: "es" }],
  plugins: [
    typescript({
      module: "ESNext",
    }),
    babel({
      babelrc: false,
      extensions,
      babelHelpers: "inline",
      comments: false,
      plugins: [
        "minify-constant-folding",
        "minify-dead-code-elimination",
        "minify-flip-comparisons",
        "minify-guarded-expressions",
        "transform-merge-sibling-variables",
        constantEval,
        require("./scripts/for-of-loose")(),
        ["@babel/plugin-proposal-optional-chaining", { loose: true }],
      ],
    }),
    node({
      extensions,
    }),
    prettier({
      parser: "babel",
      singleQuote: true,
    }),
  ],
}
