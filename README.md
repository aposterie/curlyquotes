# curlyquotes

[![dependency status](https://david-dm.org/proteriax/curlyquotes/status.svg)](https://david-dm.org/proteriax/curlyquotes#info=dependencies) [![devDependency status](https://david-dm.org/proteriax/curlyquotes/dev-status.svg)](https://david-dm.org/proteriax/curlyquotes#info=devDependencies) [![build status](https://travis-ci.org/proteriax/curlyquotes.svg?branch=master)](https://travis-ci.org/proteriax/curlyquotes) [![npm version](https://badge.fury.io/js/curlyquotes.svg)](https://badge.fury.io/js/curlyquotes)

Dynamically apply smart curly quotes to your web page.

```bash
yarn add curlyquotes
```

```javascript
import * as curlyquotes from "curlyquotes"
curlyquotes.observe()
```

The code snippet above will detect all straight quotes and convert them to
smart versions, and will continue to observe changes in the DOM tree
and apply the changes.

## Usage

### DOM

**Notes:**
The following three DOM methods of `curlyquotes` accept an optional Node, which
falls back to `document.body` if omitted.

```typescript
/**
 * Recursively walk through and convert all child nodes
 * of `rootNode`.
 */
export function init(rootNode?: Node): void

/** Starts observing the `node`. */
export function observe(node?: Node): void

/** Stops all observations started by `observe` to `node`. */
export function disconnect(node?: Node): void

/** Provide a CSS selector used to ignore matched nodes. */
export function ignore(selector: string): void

/** Returns a new string with smartquotes applied. */
export function string(source: string): string
```

## Supported Browsers

The latest three versions of all evergreen browsers.

## License

[BSD 3 Clause](LICENSE)
