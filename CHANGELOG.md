# Changelog

## 1.4.0

- Drops supports for ancient browsers.

## 1.3.4

- Supports async init

## 1.3.0

- Undocumented chaining API is removed.
- `curlyquotes.init().observe()` should be replaced with `curlyquotes.observe()`.

# Known Issues

**Problem:** Used as a userscript, `curlyquotes` will not work on [nicovideo.jp]() because
some key JSON data is stored in a `<div>`.
**Solution:** Disable it on `nicovideo.jp`, OR, add `node.offsetParent === null` test for
visibility to `shouldSkipNode` function.
