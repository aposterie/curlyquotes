import * as _ from "lodash"
import { expect } from "chai"
import { JSDOM } from "jsdom"
import dedent from "dedent"
import * as c from "../lib/index"

describe("main", () => {
  const { string, observe, init } = c
  observe

  const { document, Node, NodeFilter } = new JSDOM("").window
  Object.assign(global, { document, Node, NodeFilter })

  function jsx(tag = "div", html = "") {
    let n = document.createElement(tag)
    n.innerHTML = html
    return n
  }
  function div(html: string) {
    return jsx("div", dedent(html))
  }
  function expectText(node: HTMLElement, textContent: string) {
    expect(node.textContent!.trim()).to.equal(dedent(textContent))
  }

  describe("API", () => {
    it("should have a string manipulation function: (str) => str", () => {
      expect(string).to.be.a("function")
      expect(string("string")).to.be.a("string")
    })
  })

  describe("string samples", () => {
    it("should work for string: sample sentence 1", () => {
      expect(string(`Ma'am, this is a "quote".`)).to.equal(
        `Ma’am, this is a “quote”.`
      )
    })
  })

  describe("DOM samples", () => {
    it("should treat <span /> as inline-level element", () => {
      const p = jsx("p", /*html*/ `Ma'am, <em>that</em>'s rough.`)
      init(p)
      expectText(p, `Ma’am, that’s rough.`)
    })

    it("should treat certain CJK punctuations as a space", () => {
      const p = jsx(
        "p",
        `redditという名前の由来は、"read it"をもじった言葉遊びである。`
      )
      init(p)
      expectText(p, `redditという名前の由来は、“read it”をもじった言葉遊びである。`)
    })

    it("should consider two textNodes together", () => {
      const p = jsx("p")
      p.appendChild(document.createTextNode('"'))
      p.appendChild(document.createTextNode('"'))
      init(p)
      expectText(p, "“”")
    })

    it("should skip script tags", () => {
      const html = /*html*/ `<p>Hello</p><script>console.log("Hello World\'s!")</script>`
      const div = jsx("div", html)

      init(div)
      expect(div.innerHTML).to.equal(html)
    })

    it("should skip comments", () => {
      const html = '"Hello"'
      const comment = document.createComment(html)
      const div = jsx()
      init(comment)
      div.appendChild(comment)
      expect(div.innerHTML).to.equal(`<!--${html}-->`)
    })

    it("should skip custom selector elements", () => {
      const html = /*html*/ `<p>Hello<span class="ignore">"world"</span></p>`
      const div = jsx("div", html)
      c.ignore(".ignore")
      init(div)
      expect(div.innerHTML).to.equal(html)
      c.ignore()
    })

    it("should skip Monaco editor", () => {
      const node = div(/*html*/ `
        <div class="monaco-editor vs">
          <div class="lines-content monaco-editor-background" style data-transform>
            <div class="view-lines" role="presentation">
              <div linenumber="11" style="top:190px;height:19px;" class="view-line">
                <span>
                  <span class="token ">    </span><span class="token string js">"use strict"</span><span class="token delimiter js">;</span>
                </span>
              </div>
            </div>
          </div>
        </div>
      `)
      init(node)
      expectText(node, '"use strict";')
    })

    it("should work with Wikipedia sample texts", () => {
      const div1 = div(/*html*/ `
      <p>
        In contrast, Northerners, even moderates previously opposed to Sumner's extreme abolitionist invective, were universally shocked by Brooks's violence. Anti-slavery men cited it as evidence that the South had lost interest in national debate, and now relied on "the bludgeon, the revolver, and the bowie-knife" to display their feelings, and silence their opponents. J. L. Magee's political cartoon famously expressed the general Northern sentiment that the South's vaunted <a href="/wiki/Chivalry" title="Chivalry">chivalry</a> had degenerated into "Argument versus Clubs".
      </p>`)

      c.init(div1)
      expectText(
        div1,
        `In contrast, Northerners, even moderates previously opposed to Sumner’s extreme abolitionist invective, were universally shocked by Brooks’s violence. Anti-slavery men cited it as evidence that the South had lost interest in national debate, and now relied on “the bludgeon, the revolver, and the bowie-knife” to display their feelings, and silence their opponents. J. L. Magee’s political cartoon famously expressed the general Northern sentiment that the South’s vaunted chivalry had degenerated into “Argument versus Clubs”.`
      )

      const div2 = div(/*html*/ `
            <div id="mp-dyk">
      <div class="dyk-img" style="float: right; margin-left: 0.5em;">
      <div class="thumbinner mp-thumb" style="background: transparent; border: none; padding: 0; max-width: 120px;">
      <a href="/wiki/File:MJK_42887_Michael_Boddenberg_(Hessischer_Landtag_2019).jpg" class="image" title="Michael Boddenberg"><img alt="Michael Boddenberg" src="//upload.wikimedia.org/wikipedia/commons/thumb/0/09/MJK_42887_Michael_Boddenberg_%28Hessischer_Landtag_2019%29.jpg/120px-MJK_42887_Michael_Boddenberg_%28Hessischer_Landtag_2019%29.jpg" decoding="async" width="120" height="180" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/0/09/MJK_42887_Michael_Boddenberg_%28Hessischer_Landtag_2019%29.jpg/180px-MJK_42887_Michael_Boddenberg_%28Hessischer_Landtag_2019%29.jpg 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/0/09/MJK_42887_Michael_Boddenberg_%28Hessischer_Landtag_2019%29.jpg/240px-MJK_42887_Michael_Boddenberg_%28Hessischer_Landtag_2019%29.jpg 2x" data-file-width="3977" data-file-height="5965"></a><div class="thumbcaption" style="padding: 0.25em 0; word-wrap: break-word;">Michael Boddenberg</div></div>
      </div>
      <ul><li>... that <b><a href="/wiki/Michael_Boddenberg" title="Michael Boddenberg">Michael Boddenberg</a></b> <i>(pictured)</i>, the minister of finance of the German state of <a href="/wiki/Hesse" title="Hesse">Hesse</a>, once directed a school for butchers and bakers?</li>
      <li>... that the pencil sketch for <i><b><a href="/wiki/Little_Girl_Observing_Lovers_on_a_Train" title="Little Girl Observing Lovers on a Train">Little Girl Observing Lovers on a Train</a></b></i> is in the private collection of <a href="/wiki/George_Lucas" title="George Lucas">George Lucas</a>?</li>
      <li>... that <b><a href="/wiki/Alexander_H._Flax" title="Alexander H. Flax">Alexander H. Flax</a></b>, the future <a href="/wiki/Chief_Scientist_of_the_U.S._Air_Force" title="Chief Scientist of the U.S. Air Force">chief scientist of the U.S. Air Force</a>, was part of a small team of engineers who developed the <a href="/wiki/Piasecki_HRP_Rescuer" title="Piasecki HRP Rescuer">Piasecki HRP Rescuer</a>, the first true <a href="/wiki/Tandem_rotors" title="Tandem rotors">twin-rotor</a> helicopter?</li>
      <li>... that many <b><a href="/wiki/Postwar_anti-Jewish_violence_in_Slovakia" title="Postwar anti-Jewish violence in Slovakia">post-World War II anti-Jewish attacks in Slovakia</a></b> were committed by former anti-Nazi <a href="/wiki/Slovak_partisans" title="Slovak partisans">partisans</a>?</li>
      <li>... that <b><a href="/wiki/Carol_Brightman" title="Carol Brightman">Carol Brightman</a></b> first gained inspiration for her book <i>Sweet Chaos</i> from her younger sister, who worked as the <a href="/wiki/Grateful_Dead" title="Grateful Dead">Grateful Dead</a>'s lighting director and literary agent?</li>
      <li>... that an incident in which a player clipped another's heel at the <b><a href="/wiki/2002_PDC_World_Darts_Championship" title="2002 PDC World Darts Championship">2002 PDC World Darts Championship</a></b> was the catalyst for the creation of the exclusion zone in darts?</li>
      <li>... that <b><a href="/wiki/14_Wall_Street" title="14 Wall Street">14&nbsp;Wall Street</a></b> in Manhattan was among the first skyscrapers to use a pyramidal roof?</li>
      <li>... that <b><a href="/wiki/Albert_Camus" title="Albert Camus">Albert Camus</a></b><span class="nowrap">'s</span> novel <i><a href="/wiki/The_Plague" title="The Plague">The Plague</a></i> is based on an epidemic in <a href="/wiki/Oran" title="Oran">Oran</a>, Algeria, and examines how a government could turn tyrannical?</li></ul>
      <div class="dyk-footer hlist hlist-separated noprint" style="margin-top: 0.5em; text-align: right;">
      <ul><li><b><a href="/wiki/Wikipedia:Recent_additions" title="Wikipedia:Recent additions">Archive</a></b></li>
      <li><b><a href="/wiki/Help:Your_first_article" title="Help:Your first article">Start a new article</a></b></li>
      <li><b><a href="/wiki/Template_talk:Did_you_know" title="Template talk:Did you know">Nominate an article</a></b></li></ul>
      </div>
      </div>
      `)

      init(div2)
      expectText(
        div2,
        `Michael Boddenberg

      … that Michael Boddenberg (pictured), the minister of finance of the German state of Hesse, once directed a school for butchers and bakers?
      … that the pencil sketch for Little Girl Observing Lovers on a Train is in the private collection of George Lucas?
      … that Alexander H. Flax, the future chief scientist of the U.S. Air Force, was part of a small team of engineers who developed the Piasecki HRP Rescuer, the first true twin-rotor helicopter?
      … that many post-World War II anti-Jewish attacks in Slovakia were committed by former anti-Nazi partisans?
      … that Carol Brightman first gained inspiration for her book Sweet Chaos from her younger sister, who worked as the Grateful Dead’s lighting director and literary agent?
      … that an incident in which a player clipped another’s heel at the 2002 PDC World Darts Championship was the catalyst for the creation of the exclusion zone in darts?
      … that 14 Wall Street in Manhattan was among the first skyscrapers to use a pyramidal roof?
      … that Albert Camus’s novel The Plague is based on an epidemic in Oran, Algeria, and examines how a government could turn tyrannical?
      
      Archive
      Start a new article
      Nominate an article`
      )
    })
  })
})
