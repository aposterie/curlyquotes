import { replace } from "../src/replace"
import { expect } from "chai"

const map = {
  "&#8216;": "‘",
  "&#8217;": "’",
  "&#8220;": "“",
  "&#8221;": "”",
  "&#8211;": "–",
  "&#8212;": "—",
  "&#8230;": "…",
}
map

describe("entity", () => {
  expect(replace("The 'quick brown fox' jumps.")).to.equal(
    `The ‘quick brown fox’ jumps.`
  )
  expect(replace(`6'2" tall`)).to.equal(`6’2” tall`)
  // expect(replace("A--B")).to.equal(`A—B`)
  expect(replace('2018"')).to.equal(`2018”`)
  // expect(replace("hello ...")).to.equal(`hello …`)
})
