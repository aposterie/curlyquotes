import { SKIPPED_TAGS, SKIPPED_CLASSES, INLINE_TAGS } from "./constants"

let customSkipSelector: string | undefined
/**
 * Customize which elements to ignore.
 * Pass undefined to remove custom selector.
 */
export function ignore(selector?: string) {
  customSkipSelector = selector
}

/**
 * Returns true if node should be skipped, or node is null.
 * @internal
 */
export function shouldSkipNode(node: Element): boolean {
  if (node.nodeType === Node.ELEMENT_NODE) {
    return (
      SKIPPED_TAGS.includes(node.nodeName) ||
      SKIPPED_CLASSES.test(node.getAttribute("class") || "") ||
      node.hasAttribute("contenteditable") ||
      (customSkipSelector && node.matches(customSkipSelector)) ||
      (node.parentElement != null && shouldSkipNode(node.parentElement))
    )
  } else if (node.nodeType === Node.TEXT_NODE) {
    return !node.parentElement || shouldSkipNode(node.parentElement)
  } else {
    return true
  }
}

/**
 * Returns correct NodeFilter value for whether the node should be accepted.
 * @internal
 * @param node
 */
export function shouldAcceptNode(node: HTMLElement): number {
  return shouldSkipNode(node) ? NodeFilter.FILTER_REJECT : NodeFilter.FILTER_ACCEPT
}

/**
 * Returns true if a node should be treated as an inline element.
 * @internal
 * @param node
 */
export function isInlineElement(node: Node): boolean {
  return INLINE_TAGS.includes(node.nodeName)
}

/**
 * Returns true if the context requires curlyquotes to bail out.
 * @internal
 */
export function shouldSkipInlineContext(context: string) {
  // Node used to store JSON values
  return context[0] === "{" || context[0] === "["
}

/**
 * Returns the concatenated string of all preceding textNode within the block
 * level element that the given node is in.
 * @internal
 */
export function getInlineContext(
  node: Node | null,
  cache: WeakMap<Node, string>
): string {
  let res = ""
  while (node) {
    if (!node.previousSibling) {
      node = node.parentNode
      if (!node || !isInlineElement(node)) {
        break
      }
    } else {
      node = node.previousSibling
      if (node.nodeType === Node.ELEMENT_NODE) {
        if (!isInlineElement(node)) break
        while (node.lastChild) {
          node = node.lastChild
        }
      }
      if (cache.has(node)) {
        res = cache.get(node) + res
        break
      } else if (node.nodeType === Node.TEXT_NODE) {
        res = node.nodeValue + res
        cache.set(node, res)
      }
    }
  }
  if (node) {
    cache.set(node, res)
  }
  return res
}
