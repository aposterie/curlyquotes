/**
 * Nodes matching any of the following will be skipped for curlyquotes.
 * @internal
 */
export const SKIPPED_TAGS = [
  "INPUT",
  "PRE",
  "CODE",
  "SELECT",
  "TEXTAREA",
  "OBJECT",
  "NOSCRIPT",
  "SCRIPT",
  "STYLE",
  "LINK",
  "TEMPLATE",
]

/**
 * Nodes matching any of the following className will be skipped.
 * @internal
 */
export const SKIPPED_CLASSES = /(^|\s)(CodeMirror|monaco|highlight|hljs|crayon-line|ace_line|prism)/i

/**
 * Nodes with these tags are not considered block-level elements.
 * @internal
 */
export const INLINE_TAGS = [
  "B",
  "BIG",
  "I",
  "SMALL",
  "TT",
  "ABBR",
  "ACRONYM",
  "CITE",
  "CODE",
  "DEL",
  "DFN",
  "EM",
  "KBD",
  "STRONG",
  "SAMP",
  "VAR",
  "A",
  "BDO",
  "BR",
  "IMG",
  "MAP",
  "Q",
  "SPAN",
  "LABEL",
  "SELECT",
  "TIME",
  "FONT",
  "#text",
  undefined,
]
