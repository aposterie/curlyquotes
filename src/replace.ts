/**
 * Returns a new string with straight quotes replaced with curlyquotes.
 * @param text String with straight quotes.
 */
export function replace(text: string) {
  return text
    .replace(/---/g, "—") // — TRIPLE DASH
    .replace(/--/g, "–") // – DOUBLE DASH
    .replace(/(^|[-—/(\[{"\s，、。])'/g, "$1‘") // ‘ LEFT SINGLE
    .replace(/'/g, "’") // ’ RIGHT SINGLE
    .replace(/"(\s*)$/, "”$1")
    .replace(/(^|[-—/(\[{‘\s，、。])"/g, "$1“") // “ LEFT DOUBLE
    .replace(/"/g, "”") // ” RIGHT DOUBLE
    .replace(/\.{3}/g, "…")
}
