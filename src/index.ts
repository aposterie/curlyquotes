import { replace } from "./replace"
import {
  shouldSkipNode,
  shouldAcceptNode as acceptNode,
  getInlineContext,
  shouldSkipInlineContext,
} from "./dom"

export { ignore } from "./dom"
export { replace }

// Hack to get rid of infinite loop
let done = new WeakSet()

/** Observer */
const records = new Set<{ node: Node; observer: MutationObserver }>()

interface Options {
  async: boolean
}

const nextTick = () => new Promise((resolve) => setTimeout(resolve))

// Old API
export { replace as string }

/**
 * Apply a Node and all its descendants curly quotes. This action is static
 * and will not affect future nodes until executed again.
 * @param node Root node
 */
export async function init(node: Node = document.body, { async = false } = {}) {
  if (shouldSkipNode(node as Element)) return
  const iterator = document.createNodeIterator(node, NodeFilter.SHOW_TEXT, {
    acceptNode,
  })

  const map = new WeakMap<Node, string>()

  while ((node = iterator.nextNode()!)) {
    if (done.has(node)) continue
    const value = node.nodeValue
    if (value && /['"-.]/.test(value)) {
      const context = getInlineContext(node, map)
      if (shouldSkipInlineContext(context)) {
        continue
      }
      const result = replace(context + value).slice(context.length)
      if (result !== value) {
        node.nodeValue = result
      }
    }

    done.add(node)

    if (async) {
      await nextTick()
    }
  }

  done = new WeakSet()
}

/**
 * Handles MutationRecords.
 * @param mutations
 */
function observerCallback(mutations: MutationRecord[]): void {
  for (const { type, addedNodes, target } of mutations) {
    if (type === "childList") {
      for (const node of <any>addedNodes) {
        init(node)
      }
    } else if (type === "characterData") {
      ;/['"-.]/.test(target.nodeValue!) && init(target)
    }
  }
}

/**
 * Attaches observer to a Node to automatically apply curly quotes
 * when its contents change.
 * Returns a function used to stop listening.
 * @param node Root Node
 */
export function observe(node: Node = document.body, options?: Options) {
  const observer = new MutationObserver(observerCallback)
  observer.observe(node, {
    characterData: true,
    childList: true,
    subtree: true,
  })
  records.add({ node, observer })
  init(node, options)
  return () => disconnect(node)
}

/**
 * Removes observer added by `observe` function.
 * @param node RootNode
 */
export function disconnect(node: Node = document.body) {
  records.forEach((record) => {
    if (record.node === node) {
      record.observer.disconnect()
      records.delete(record)
    }
  })
}
